﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;       //Public variable to store a reference to the player game object
    public Vector3 offset;         //Private variable to store the offset distance between the player and camera
    public float smoothSpeed = 5f;
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;

    // Use this for initialization
    void Start () {
        offset = transform.position - player.transform.position;
    }
	


    void LateUpdate()
    {
        /*   Vector3 desiredPos = player.transform.position + offset;
           Vector3 smoothPos = Vector3.SmoothDamp(transform.position,desiredPos, ref velocity, smoothTime);
           transform.position = smoothPos;*/
        transform.position = player.transform.position + offset;
    }
}
