﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotBullet2 : MonoBehaviour {

    GameObject bot2;

    //    GameObject player;
    public float speed = 5f;
    private void Awake()
    {
   //       player = GameObject.FindGameObjectWithTag("player");
    //    bot0 = GameObject.FindGameObjectWithTag("bot0");
    }

    //When bullet is enabled, call Destroy method in 2 secs
    private void OnEnable()
    {
        bot2 = GameObject.FindGameObjectWithTag("bot2");
        Invoke("Done", 2f);       
    }
    float distBetweenBulletAndBot2;
    private void Update()
    {
            transform.Translate(speed + Time.deltaTime, 0, 0);          
            distBetweenBulletAndBot2 = Vector3.Distance(transform.position, bot2.transform.position);
            if (distBetweenBulletAndBot2 > 15)
            {
                Done();
            }
        
    }

    void Done()
    {
        this.gameObject.SetActive(false);
    }


    private void OnTriggerEnter (Collider other)
    {
        if(other.gameObject.tag == "bot1" || other.gameObject.tag == "bot0" || other.gameObject.tag == "player")
        {
       //     distBetweenBulletAndBot2 = Vector3.Distance(bot2.transform.position, other.gameObject.transform.position);
                 if (distBetweenBulletAndBot2 < 2)
                     other.gameObject.GetComponent<Rigidbody>().AddForce(bot2.GetComponent<RandomMovement>().moveDirection * 1000);

                 if (distBetweenBulletAndBot2 > 2 && distBetweenBulletAndBot2 < 7)
                     other.gameObject.GetComponent<Rigidbody>().AddForce(bot2.GetComponent<RandomMovement>().moveDirection * 400);

                 if (distBetweenBulletAndBot2 > 7 && distBetweenBulletAndBot2 < 15)
                     other.gameObject.GetComponent<Rigidbody>().AddForce(bot2.GetComponent<RandomMovement>().moveDirection * 50);

            PlayerPrefs.SetString(other.gameObject.tag, "bot2");

            Done();
        }        
    }
}
