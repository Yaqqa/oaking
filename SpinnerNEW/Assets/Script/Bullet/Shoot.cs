﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour {

      public float fireTime = 0.05f;
    float firstTime;
    GameManager manager;
 /*   List<GameObject> bullets;
    List<GameObject> bullets2;
    List<GameObject> bullets3;*/
 //   ObjectPooler OP;


    //MOD 2 shooting
    public float velocity;
    public Material stuffMaterial;
    public Bullet[] stuffPrefabs;
    float timeSinceLastSpawn;
    float currentSpawnDelay;
    float next_spawn_time;
    public float BulletForcePlayer = 800;
    void Start()
    {
        firstTime = fireTime;
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        next_spawn_time = Time.time + 0.03f;

    }

    public void FireTrigger()
    {
        InvokeRepeating("Fire", fireTime, fireTime);
    }

    public void EndFireTrigger()
    {
        CancelInvoke("Fire");
    }

    
    private void Update()
    {
        if (manager.playing)
        {
            if (Time.time > next_spawn_time)
            {
                SpawnBullet();
                next_spawn_time += 0.03f;
            }
                
        }
    }


    Vector3 playerPos;
    Vector3 playerDirection;
    Quaternion playerRotation;
    float spawnDistance = 1.5f;

    void SpawnBullet()
    {
     //   spawnDistance = GetComponent<CapsuleCollider>().radius * transform.localScale.x;
        spawnDistance = 1.5f * transform.localScale.x;
        playerPos = transform.position;
        playerDirection = transform.forward;
        playerRotation = transform.rotation;
        Vector3 spawnPos = playerPos + playerDirection * spawnDistance;

        Bullet prefab = stuffPrefabs[0];
        Bullet spawn = prefab.GetPooledInstance<Bullet>();
        spawn.transform.localPosition = spawnPos;
        spawn.transform.rotation = playerRotation;
     //   spawn.transform.localPosition = transform.position + new Vector3(0, 0, 5f);
     //  spawn.transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
     //   spawn.transform.rotation = transform.rotation;
        spawn.bulletID = 0;

        Bullet prefab2 = stuffPrefabs[1];
        Bullet spawn2 = prefab2.GetPooledInstance<Bullet>();
        //     spawn2.transform.localPosition = spawnPos + transform.right * 1.3f;
        spawn2.transform.localPosition = spawnPos;
        spawn2.transform.rotation = playerRotation;
      //    spawn2.transform.rotation = Quaternion.Euler(playerRotation.x, playerRotation.y+20, playerRotation.z) ;
        //   spawn2.transform.localPosition = transform.position + new Vector3(1.5f,0,5f);
        //   spawn2.transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
        spawn2.bulletID = 0;

        Bullet prefab3 = stuffPrefabs[2];
        Bullet spawn3 = prefab3.GetPooledInstance<Bullet>();
        //    spawn3.transform.localPosition = spawnPos + transform.right * -1.3f;
        spawn3.transform.localPosition = spawnPos;
        spawn3.transform.rotation = playerRotation;
    //    spawn3.transform.rotation = Quaternion.Euler(playerRotation.x, playerRotation.y - 20, playerRotation.z);
        //   spawn3.transform.localPosition = transform.position + new Vector3(-1.5f, 0, 5f);
        //   spawn3.transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
        //   spawn3.transform.rotation = transform.rotation;
        spawn3.bulletID = 0;

    }

}
