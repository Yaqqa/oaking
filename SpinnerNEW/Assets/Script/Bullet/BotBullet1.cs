﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotBullet1 : MonoBehaviour {

    GameObject bot1;

    //    GameObject player;
    public float speed = 5f;
    private void Awake()
    {
   //       player = GameObject.FindGameObjectWithTag("player");
    //    bot0 = GameObject.FindGameObjectWithTag("bot0");
    }

    //When bullet is enabled, call Destroy method in 2 secs
    private void OnEnable()
    {
        bot1 = GameObject.FindGameObjectWithTag("bot1");
        Invoke("Done", 2f);       
    }
    float distBetweenBulletAndBot1;
    private void Update()
    {
            transform.Translate(speed + Time.deltaTime, 0, 0);
            
            distBetweenBulletAndBot1 = Vector3.Distance(transform.position, bot1.transform.position);
            if (distBetweenBulletAndBot1 > 15)
            {
                Done();
            }
        
    }

    void Done()
    {
        this.gameObject.SetActive(false);
    }


    private void OnTriggerEnter (Collider other)
    {
        if(other.gameObject.tag == "bot0" || other.gameObject.tag == "bot1" || other.gameObject.tag == "player")
        {
         //   distBetweenBulletAndBot1 = Vector3.Distance(bot1.transform.position, other.gameObject.transform.position);
                 if (distBetweenBulletAndBot1 < 2)
                     other.gameObject.GetComponent<Rigidbody>().AddForce(bot1.GetComponent<RandomMovement>().moveDirection * 1000);

                 if (distBetweenBulletAndBot1 > 2 && distBetweenBulletAndBot1 < 7)
                     other.gameObject.GetComponent<Rigidbody>().AddForce(bot1.GetComponent<RandomMovement>().moveDirection * 400);

                 if (distBetweenBulletAndBot1 > 7 && distBetweenBulletAndBot1 < 15)
                     other.gameObject.GetComponent<Rigidbody>().AddForce(bot1.GetComponent<RandomMovement>().moveDirection * 50);

            PlayerPrefs.SetString(other.gameObject.tag, "bot2");

            Done();
        }
        
    }
}
